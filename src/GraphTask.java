import java.util.*;

/** Container class to different classes, that makes the whole
 * set of classes one class formally.
 */
public class GraphTask {

   /** Main method. */
   public static void main (String[] args) {
      GraphTask a = new GraphTask();
      a.run();
      //throw new RuntimeException ("Nothing implemented yet!"); // delete this
   }

   /** Actual main method to run examples and everything. */
   public void run() {
      Graph g = new Graph ("Graaf 1");
      Graph g2 = new Graph ("Graaf 2");

      g.createRandomSimpleGraph(2000, 3000);
      long start = System.currentTimeMillis();
      System.out.println(g.widestPath(g.first, g.first.next));
      System.out.println(String.format("Aega kulus %d ms",  System.currentTimeMillis() - start));

      /*Vertex a = g2.createVertex("A");
      Vertex b = g2.createVertex("B");
      Vertex c = g2.createVertex("C");
      Vertex d = g2.createVertex("D");
      Vertex f = g2.createVertex("F");

      g2.createArc("AB", a, b, 2);
      g2.createArc("AD", a, d, 10);
      g2.createArc("AF",  a, f, 8);

      g2.createArc("BA", b, a, 2);
      g2.createArc("BC", b, c, 8);

      g2.createArc("DA", d, a, 10);
      g2.createArc("DC", d, c, 7);
      g2.createArc("DF",  d, f, 4);

      g2.createArc("CD", c, d, 7);
      g2.createArc("CB", c, b, 8);

      g2.createArc("FA",  f, a, 8);
      g2.createArc("FD",  f, d, 4);

      System.out.println(g2.widestPath(f, c));*/

      /*Vertex a = g2.createVertex("A");
      Vertex b = g2.createVertex("B");
      Vertex c = g2.createVertex("C");
      Vertex d = g2.createVertex("D");
      Vertex f = g2.createVertex("F");

      g2.createArc("AB", a, b, 2);
      g2.createArc("AD", a, d, 10);
      g2.createArc("AF",  a, f, 6);

      g2.createArc("BA", b, a, 2);
      g2.createArc("BC", b, c, 8);

      g2.createArc("DA", d, a, 10);
      g2.createArc("DC", d, c, 7);
      g2.createArc("DF",  d, f, 6);

      g2.createArc("CD", c, d, 7);
      g2.createArc("CB", c, b, 8);

      g2.createArc("FA",  f, a, 6);
      g2.createArc("FD",  f, d, 6);

      System.out.println(g2.widestPath(b, a));*/

      /*Vertex a = g2.createVertex("A");
      Vertex b = g2.createVertex("B");
      Vertex c = g2.createVertex("C");
      Vertex d = g2.createVertex("D");

      /g2.createArc("AB", a, b, 2);
      g2.createArc("AD", a, d, 1);

      g2.createArc("BA", b, a, 2);
      g2.createArc("BC", b, c, 5);

      g2.createArc("DA", d, a, 1);
      g2.createArc("DC", d, c, 7);

      g2.createArc("CD", c, d, 7);
      g2.createArc("CB", c, b, 5);

      System.out.println(g2.widestPath(b, a));*/

      /*Vertex a = g2.createVertex("A");
      Vertex b = g2.createVertex("B");
      Vertex c = g2.createVertex("C");
      Vertex d = g2.createVertex("D");
      Vertex f = g2.createVertex("F");

      g2.createArc("AB", a, b, 2);
      g2.createArc("AD", a, d, 1);
      g2.createArc("AF",  a, f, 6);

      g2.createArc("BA", b, a, 2);
      g2.createArc("BC", b, c, 5);

      g2.createArc("DA", d, a, 1);
      g2.createArc("DC", d, c, 7);
      g2.createArc("DF",  d, f, 6);

      g2.createArc("CD", c, d, 7);
      g2.createArc("CB", c, b, 5);

      g2.createArc("FA",  f, a, 6);
      g2.createArc("FD",  f, d, 6);*/

      /*Vertex maldon = g.createVertex("Maldon");
      Vertex clacton = g.createVertex("Clacton");
      Vertex tiptree = g.createVertex("Tiptree");
      Vertex harwich = g.createVertex("Harwich");
      Vertex feering = g.createVertex("Feering");
      Vertex blaxhall = g.createVertex("Blaxhall");
      Vertex dunwich = g.createVertex("Dunwich");

      g.createArc("Maldon-Tiptree", maldon, tiptree, 8);
      g.createArc("Tiptree-Maldon", tiptree, maldon, 8);

      g.createArc("Maldon-Clacton", maldon, clacton, 40);
      g.createArc("Clacton-Maldon", clacton, maldon, 40);

      g.createArc("Tiptree-Clacton", tiptree, clacton, 29);
      g.createArc("Clacton-Tiptree", clacton, tiptree, 29);
      g.createArc("Tiptree-Harwich", tiptree, harwich, 31);
      g.createArc("Harwich-Tiptree", harwich, tiptree, 31);

      g.createArc("Clacton-Harwich", clacton, harwich, 17);
      g.createArc("Harwich-Clacton", harwich, clacton, 17);

      g.createArc("Maldon-Feering", maldon, feering, 11);
      g.createArc("Feering-Maldon", feering, maldon, 11);
      g.createArc("Feering-Tiptree", feering, tiptree, 3);
      g.createArc("Tiptree-Feering", tiptree, feering, 3);

      g.createArc("Feering-Blaxhall", feering, blaxhall, 46);
      g.createArc("Blaxhall-Feering", blaxhall, feering, 46);

      g.createArc("Harwich-Blaxhall", harwich, blaxhall, 40);
      g.createArc("Blaxhall-Harwich", blaxhall, harwich, 40);

      g.createArc("Harwich-Dunwich", harwich, dunwich, 53);
      g.createArc("Dunwich-Harwich", dunwich, harwich, 53);

      g.createArc("Dunwich-Blaxhall", dunwich, blaxhall, 15);
      g.createArc("Blaxhall-Dunwich", blaxhall, dunwich, 15);
      //System.out.println(g.widestPath(maldon, feering));*/

      //g.createRandomSimpleGraph (6, 9);

      // TODO!!! Your experiments here
   }

   // TODO!!! add javadoc relevant to your problem
   class Vertex /*implements Comparable<Vertex>*/{

      private String id;
      private Vertex next;
      private Arc first;
      private int info = 0;
      private int widestDistanceTo = Integer.MIN_VALUE;
      // You can add more fields, if needed

      Vertex (String s, Vertex v, Arc e) {
         id = s;
         next = v;
         first = e;
      }

      Vertex (String s) {
         this (s, null, null);
      }

      /*@Override
      public int compareTo(Vertex other) {
         return Integer.compare(other.widestDistanceTo, this.widestDistanceTo);
      }*/

      @Override
      public String toString() {
         return id;
      }

      // TODO!!! Your Vertex methods here!
   }


   /** Arc represents one arrow in the graph. Two-directional edges are
    * represented by two Arc objects (for both directions).
    */
   class Arc{

      private String id;
      private Vertex target;
      private Arc next;
      private int info = 0;
      public int weight = 0;
      // You can add more fields, if needed

      Arc (String s, Vertex v, Arc a) {
         id = s;
         target = v;
         next = a;
      }


      Arc (String s) {
         this (s, null, null);
      }

      @Override
      public String toString() {
         return id;
      }


      // TODO!!! Your Arc methods here!
   } 

   class Graph {

      private String id;
      private Vertex first;
      private int info = 0;

      Graph (String s, Vertex v) {
         id = s;
         first = v;
      }

      Graph (String s) {
         this (s, null);
      }

      /**
       * Calculates and returns the maximum bandwidth between two vertices.
       * @param from start vertex
       * @param to end vertex
       */

      public int widestPath(Vertex from, Vertex to) {


         from.widestDistanceTo = Integer.MAX_VALUE;

         List<Arc> arcsFromVertexSorted = new ArrayList<>();
         PriorityQueue<Vertex> pq = new PriorityQueue<Vertex>(Comparator.comparingInt(x -> x.widestDistanceTo));

         pq.add(from);
         while (pq.size() > 0) {

            arcsFromVertexSorted.clear();
            Vertex current = pq.remove();

            Arc a = current.first;
            while (a != null) {
               arcsFromVertexSorted.add(a);
               a = a.next;
            }
            Collections.sort(arcsFromVertexSorted, Comparator.comparingInt(x -> x.weight));
            for (Arc b : arcsFromVertexSorted) {

               if(b.weight > b.target.widestDistanceTo && current.widestDistanceTo > b.target.widestDistanceTo){

                  if(b.weight < current.widestDistanceTo)
                     b.target.widestDistanceTo = b.weight;

                  else
                  {
                     if(current == from)
                        b.target.widestDistanceTo = b.weight;

                     else
                        b.target.widestDistanceTo = current.widestDistanceTo;
                  }
                  pq.add(b.target);
               }
            }
         }
         if(to == from) return 0;
         else if(to.widestDistanceTo == Integer.MIN_VALUE) return -1;
         return to.widestDistanceTo;
      }

      @Override
      public String toString() {
         String nl = System.getProperty ("line.separator");
         StringBuffer sb = new StringBuffer (nl);
         sb.append (id);
         sb.append (nl);
         Vertex v = first;
         while (v != null) {
            sb.append (v.toString());
            sb.append (" -->");
            Arc a = v.first;
            while (a != null) {
               sb.append (" ");
               sb.append (a.toString());
               sb.append (" (");
               sb.append (v.toString());
               sb.append ("->");
               sb.append (a.target.toString());
               sb.append (")");
               a = a.next;
            }
            sb.append (nl);
            v = v.next;
         }
         return sb.toString();
      }

      public Vertex createVertex (String vid) {
         Vertex res = new Vertex (vid);
         res.next = first;
         first = res;
         //System.out.println(dict["A"]);
         return res;
      }

      public Arc createArc (String aid, Vertex from, Vertex to) {
         Arc res = new Arc (aid);
         res.next = from.first;
         from.first = res;
         res.target = to;
         return res;
      }

      public Arc createArc (String aid, Vertex from, Vertex to, int weight) {
         Arc res = new Arc (aid);
         res.next = from.first;
         from.first = res;
         res.target = to;
         res.weight = weight;
         return res;
      }

      /**
       * Create a connected undirected random tree with n vertices.
       * Each new vertex is connected to some random existing vertex.
       * @param n number of vertices added to this graph
       */
      public void createRandomTree (int n) {
         if (n <= 0)
            return;
         Vertex[] varray = new Vertex [n];
         for (int i = 0; i < n; i++) {
            varray [i] = createVertex ("v" + String.valueOf(n-i));
            if (i > 0) {
               int vnr = (int)(Math.random()*i);
               int weight = (int )(Math.random() * 50 + 1);
               createArc ("a" + varray [vnr].toString() + "_"
                  + varray [i].toString(), varray [vnr], varray [i], weight);
               createArc ("a" + varray [i].toString() + "_"
                  + varray [vnr].toString(), varray [i], varray [vnr], weight);
            } else {}
         }
      }

      /**
       * Create an adjacency matrix of this graph.
       * Side effect: corrupts info fields in the graph
       * @return adjacency matrix
       */
      public int[][] createAdjMatrix() {
         info = 0;
         Vertex v = first;
         while (v != null) {
            v.info = info++;
            v = v.next;
         }
         int[][] res = new int [info][info];
         v = first;
         while (v != null) {
            int i = v.info;
            Arc a = v.first;
            while (a != null) {
               int j = a.target.info;
               res [i][j]++;
               a = a.next;
            }
            v = v.next;
         }
         return res;
      }

      /**
       * Create a connected simple (undirected, no loops, no multiple
       * arcs) random graph with n vertices and m edges.
       * @param n number of vertices
       * @param m number of edges
       */
      public void createRandomSimpleGraph (int n, int m) {
         if (n <= 0)
            return;
         if (n > 2500)
            throw new IllegalArgumentException ("Too many vertices: " + n);
         if (m < n-1 || m > n*(n-1)/2)
            throw new IllegalArgumentException 
               ("Impossible number of edges: " + m);
         first = null;
         createRandomTree (n);       // n-1 edges created here
         Vertex[] vert = new Vertex [n];
         Vertex v = first;
         int c = 0;
         while (v != null) {
            vert[c++] = v;
            v = v.next;
         }
         int[][] connected = createAdjMatrix();
         int edgeCount = m - n + 1;  // remaining edges
         while (edgeCount > 0) {
            int i = (int)(Math.random()*n);  // random source
            int j = (int)(Math.random()*n);  // random target

            int weight = (int )(Math.random() * 50 + 1);
            //System.out.println(weight);
            if (i==j) 
               continue;  // no loops
            if (connected [i][j] != 0 || connected [j][i] != 0) 
               continue;  // no multiple edges
            Vertex vi = vert [i];
            Vertex vj = vert [j];
            createArc ("a" + vi.toString() + "_" + vj.toString(), vi, vj, weight);
            connected [i][j] = 1;
            createArc ("a" + vj.toString() + "_" + vi.toString(), vj, vi, weight);
            connected [j][i] = 1;
            edgeCount--;  // a new edge happily created
         }
      }

      // TODO!!! Your Graph methods here! Probably your solution belongs here.
   }

} 

